# Basic LoRaWAN project for RAK3172

Test project to use an up to date LoRaWAN stack from ST with [RAK3171](https://www.rakwireless.com/en-us/products/lpwan-modules/rak3172-wisduo-lpwan-module), module for an [STM32WLE5CC](https://www.st.com/en/microcontrollers-microprocessors/stm32wle5cc.html) LoRa SoC.

--------------------

# Software

Only `make` is required to compile the project. EUIs can be edited directly in CubeMX, or in file `LoRaWAN/App/lora_app.c`.  
A VSCode `launch.json` file is provided to debug the project thanks to [Cortex-Debug](https://github.com/Marus/cortex-debug) extension. Please make sure that you are using OpenOCD `0.11.0+` as it brings `STM32WLXX` compatibility.

* STM32CubeMX: v6.3.0
* ST Stack: v1.1.0
 
# Hardware
[RAK3272](https://store.rakwireless.com/products/wisduo-breakout-board-rak3272s) breakout board, STM32 Nucleo header as ST-Link programmer. Pinout:

| Nucleo CN4 | RAK3172S |
| ---------- | -------- |
| 2          | SWCLK    |
| 3          | GND      |
| 4          | SWIO     |
| 5          | RST      |

The breakout board can be powered by the Nucleo thanks to pin `P1[1]`.